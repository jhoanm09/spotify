import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class SpotifyService {
  constructor(private http: HttpClient) {
    console.log("Spotify service is ready");
  }

  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${query}`;

    const token =
      "BQBi7shcaYm-9c6UcBsTRpyrjyIbZWfR5pev2kTmKRkEW9pPxVfGrrWRafA1JkyP3Jm3dgpqJLPJu56iq20";
    const headers = new HttpHeaders({
      Authorization: `Bearer ${token}`
    });

    return this.http.get(url, { headers });
  }

  getNewReleases() {
    return this.getQuery("browse/new-releases?limit=20").pipe(
      map(data => data["albums"].items)
    );
  }

  getArtist(term: string) {
    return this.getQuery(
      `search?q=${term}&type=artist&market=CO&limit=15`
    ).pipe(map(data => data["artists"].items));
  }
}
